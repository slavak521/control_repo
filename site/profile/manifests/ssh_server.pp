class profile::ssh_server {
    package {'openssh-server':
        ensure => present,
    }
    service { 'sshd':
        ensure => 'running',
        enable => 'true',
    }
    ssh_authorized_key { 'root@master.puppet.vm':
        ensure => present,
        user   => 'root',
        type   => 'ssh-rsa',
        key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQCqMZVHXFzjH6ow9vDtXBV8iXwO5wbZbCaFip1gidzC/LXK9BlsVVyVWxxu/g1Y85rw48JnzMx4oAIwjRo/ntjoqojA9Hf2QgjP20RmMtcC8eVKXn+HYpbT91iQ7IS4TbpB2Bf1wp+In2w1+WIuOHRN9AO0IUKZnMbdXiM3TKqSBUKzJoONK97nh5vyQdF7VwQntM5QLnO4XrnddfMuGBeDPq3w6PeSSX5sAD78bRnecE2+uxvqRMGeX9eFMWH8dfOQE9tO+IJsbUtg4falqntDSnR0dfOBDuCEr+bS8fCOzihpFvoFsybUvXW2V8FUfXwIzinquStknbfc2y2g40s1',
    }
}
